#
# Configuration file for the Sphinx documentation builder.
# http://www.sphinx-doc.org/en/stable/config

import platform
from datetime import datetime
from zoneinfo import ZoneInfo

import sphinx
import sphinx_material

project = "CBL Grenoble 2020-2029"
html_title = project
html_logo = "images/cbl_grenoble_logo.webp"
author = "Scribe RAAR"
now = datetime.now(tz=ZoneInfo("Europe/Paris"))
version = f"{now.year}-{now.month:02}-{now.day:02} {now.hour:02}H ({now.tzinfo})"

today=version

extensions = [
    "sphinx.ext.intersphinx",
]

intersphinx_mapping = {
    "wwp": ("https://luttes.frama.io/pour/la-paix/womenwagepeace", None),
    "guerrieres": ("https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/", None),
    "chinsky": ("https://judaism.gitlab.io/floriane_chinsky/", None),
    "conflitpal": ("https://conflits.frama.io/israel-palestine/israel-palestine-2023/", None),
    "sionisme": ("https://israel.frama.io/sionisme-antisionisme/", None),
    "antisem": ("https://luttes.frama.io/contre/l-antisemitisme/", None),
    "raar_2023": ("https://antiracisme.frama.io/infos-2023", None),
    "raar_2024": ("https://antiracisme.frama.io/infos-2024", None),
    "raar_liens": ("https://antiracisme.frama.io/linkertree/", None),    
    "golem": ("http://collectif-golem.frama.io/golem", None),
    "extremes": ("https://luttes.frama.io/contre/les-extremes-droites/", None),
    "judaisme_2024": ("https://judaism.gitlab.io/judaisme-2024", None),
    "cbl_liens": ("https://cbl.frama.io/cbl-grenoble/linkertree", None),    
    "parents": ("https://luttes.frama.io/pour/la-paix/parentscirclefriends", None),    
}
# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", ".venv"]
html_static_path = ["_static"]
html_show_sourcelink = True
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
extensions.append("sphinx_material")
html_theme_path = sphinx_material.html_theme_path()
html_context = sphinx_material.get_html_context()
html_theme = "sphinx_material"

extensions += ["sphinx.ext.todo"]
todo_include_todos = True

# material theme options (see theme.conf for more information)
# https://gitlab.com/bashtage/sphinx-material/blob/master/sphinx_material/sphinx_material/theme.conf
# Colors
# The theme color for mobile browsers. Hex color.
# theme_color = #3f51b5
# Primary colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange,
# brown, grey, blue-grey, white
# Accent colors:
# red, pink, purple, deep-purple, indigo, blue, light-blue, cyan,
# teal, green, light-green, lime, yellow, amber, orange, deep-orange
# color_accent = blue
# color_primary = blue-grey

html_theme_options = {
    "base_url": "https://cbl.frama.io/cbl-grenoble/decennie-2020",
    "repo_url": " https://framagit.org/cbl/cbl-grenoble/decennie-2020",
    "repo_name": project,
    "html_minify": False,
    "html_prettify": True,
    "css_minify": True,
    "repo_type": "gitlab",
    "globaltoc_depth": -1,
    "color_primary": "deep-purple",
    "color_accent": "blue",
    "theme_color": "red",
    "nav_title": f"{project} ({today})",
    "master_doc": False,
    "nav_links": [
        {
            "href": "genindex",
            "internal": True,
            "title": "Index",
        },
        {
            "href": "https://cbl.frama.io/cbl-grenoble/linkertree/",
            "internal": False,
            "title": "Liens CBL Grenoble",
        },
        {
            "href": "https://antiracisme.frama.io/linkertree/",
            "internal": False,
            "title": "Liens antiracisme",
        },
    ],
    "heroes": {
        "index": "CBL Grenoble 2020-2029",
    },
    "table_classes": ["plain"],
}
# https://github.com/sphinx-contrib/yasfb
extensions.append("yasfb")
feed_base_url = html_theme_options["base_url"]
feed_author = "Scribe"
# https://sphinx-design.readthedocs.io/en/furo-theme/get_started.html
extensions.append("sphinx_design")
# https://sphinx-tags.readthedocs.io/en/latest/quickstart.html#installation
extensions.append("sphinx_tags")
tags_create_tags = True
# Whether to display tags using sphinx-design badges.
tags_create_badges = True
extensions += ["sphinxcontrib.youtube"]

language = "en"
html_last_updated_fmt = ""

todo_include_todos = True

html_use_index = True
html_domain_indices = True

copyright = f"2020-{now.year}, {author} Built with sphinx {sphinx.__version__} Python {platform.python_version()}"

rst_prolog = """
.. |AlbertHerszkowicz| image:: /images/albert_herszkowicz_avatar.png
.. |AlbertHerszkowicz2024| image:: /images/albert_herszkowicz_avatar_2024.webp
.. |FabienneMessica| image:: /images/fabienne_messica_avatar.png
.. |Pornographes| image:: /images/pornographes_avatar.png
.. |Nadine Herrati| image:: /images/nadine_herrati.jpg
.. |Nathalie Fromont| image:: /images/nathalie_fromont.png
.. |Gregory Gutierez| image:: /images/gregory_gutierez.jpg
.. |Nathan Guedj| image:: /images/nathan_guedj.jpg
.. |logo| image:: /images/logo_avatar.png
.. |jjr| image:: /images/jjr_avatar_2024.webp
.. |raar| image:: /images/raar_avatar.png
.. |ldh| image:: /images/ldh_avatar.png
.. |halte| image:: /images/halte_antisemitisme_avatar.jpg
.. |eelv| image:: /images/eelv_avatar.png
.. |memorial98| image:: /images/memorial_98_avatar.png
.. |golem| image:: /images/golem_avatar.png
.. |nadav| image:: /images/nadav_avatar.png
.. |RobertHirsch| image:: /images/robert_hirsch_avatar.png
.. |JuifsAllemands| image:: /images/juifs_allemands_avatar.png
.. |GaucheEtJuives| image:: /images/la_gauche_et_les_juifs_avatar.png
.. |SudEducation| image:: /images/sud_education_avatar.png
.. |NonnaMayer| image:: /images/nonna_mayer_avatar.png
.. |CNCDH| image:: /images/cncdh_avatar.png
.. |vivian| image:: /images/vivian_avatar.png
.. |hanna| image:: /images/hanna_assouline_g_avatar.png
.. |daadouch| image:: /images/daadouch_avatar.webp
.. |olia| image:: /images/olia_avatar.webp
.. |visa| image:: /images/visa_avatar.webp
.. |guerrieres| image:: /images/guerrieres_de_la_paix_avatar.png
.. |gisti| image:: /images/gisti_avatar.webp
.. |GolemTheatre| image:: /images/golem_theatre.webp
.. |martin| image:: /images/martin_eden_avatar.webp
.. |oraaj_old| image:: /images/oraaj_avatar.png
.. |oraaj| image:: /images/oraaj_avatar.webp
.. |cnt_ait| image:: /images/cnt_ait_avatar.webp
.. |jcall_avatar| image:: /images/jcall_avatar.webp
.. |twitter_avatar| image:: /images/twitter_avatar.webp
.. |reseau_toxique| image:: /images/reseau_toxique.webp
.. |memorial_shoah| image:: /images/memorial_shoah_avatar.webp
.. |ava| image:: /images/ava_avatar.webp
.. |sender| image:: /images/sender_avatar.webp
.. |EmmanuelSanders| image:: /images/emmanuel_sanders_avatar.webp
.. |joshua| image:: /images/joshua_avatar.webp
.. |reibell| image:: /images/frederique_reibell_avatar.webp
.. |corcuff| image:: /images/corcuff_avatar.webp
.. |PhilippeCorcuff| image:: /images/corcuff_avatar.webp
.. |timsit| image:: /images/alice_timsit_avatar.webp
.. |emmanuel_sanders| image:: /images/emmanuel_sanders_avatar.webp
.. |BrigitteStora| image:: /images/brigitte_stora_avatar.webp
.. |DavidQuesemand| image:: /images/david_quesemand_avatar.webp
.. |lorenzo| image:: /images/lorenzo_avatar.webp
.. |RaphaelAssouline| image:: /images/raphael_assouline_avatar.webp
.. |PhilippeMarliere| image:: /images/philippe_marliere_avatar.webp
.. |JonasPardo| image:: /images/jonas_pardo_avatar.webp
.. |MartineLeibovici| image:: /images/martine_leibovici_avatar.webp
.. |YaelLerer| image:: /images/yael_lerer_avatar.webp
.. |YvesColeman| image:: /images/yves_coleman_avatar.webp
.. |AngelaRostas| image:: /images/angela_rostas_avatar.webp
.. |RaarCommunique| image:: /images/raar_communique.webp
.. |dai| image:: /images/dai_avatar.webp
.. |floriane| image:: /images/floriane_chinsky_avatar.jpg
.. |MemorialNomades| image:: /images/memorial_nomades_avatar.webp
.. |angcv| image:: /images/angcv_avatar.webp
.. |FelicienFaury| image:: /images/felicien_faury_avatar.webp
.. |EmmanuelRevah| image:: /images/emmanuel_revah_avatar.webp
.. |JanFeigen| image:: /images/jan_feigen_avatar.webp
.. |DoigtDhonneur| image:: /images/doigt_d_honneur_avatar.webp
.. |TalPiterbrautMerx| image:: /images/tal_piterbraut_merx_avatar.webp
.. |ArieAlimi| image:: /images/arie_alimi_avatar.webp
.. |CblGrenoble| image:: /images/cbl_grenoble_avatar.webp
.. |Grenoble| image:: /images/grenoble_avatar.webp
.. |YonathanZeigen| image:: /images/yonathan_zeigen.webp
.. |FatymLayachi| image:: /images/fatym_layachi_avatar.webp
.. |YuvalRahami| image:: /images/yuval_rahami_avatar.webp
.. |LilianeLevy| image:: /images/liliane_levy_avatar.webp
.. |FluxWeb| image:: /images/rss_avatar.webp
"""
