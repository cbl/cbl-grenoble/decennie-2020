.. index::
   pair: Golem Théâtre ; ROUAGES Un homme et une femme dans la tourmente des procès staliniens en Tchécoslovaquie (2024-01-20)
   ! Golem Théâtre

.. _golem_theatre_2024_01_20:

==============================================================================================================================================
2024-01-20  Golem théâtre **ROUAGES Un homme et une femme dans la tourmente des procès staliniens en Tchécoslovaquie**
==============================================================================================================================================

- https://hoteleuropa.fr/
- https://hoteleuropa.fr/rouages/
- https://hoteleuropa.fr/la-compagnie-golem-theatre/


Dossier au format PDF
=========================

- https://hoteleuropa.fr/wp-content/uploads/2023/05/Rouages-dossier-DEF.pdf

Compagnie Golem Théâtre
============================================

Créée à Prague par Michal Laznovsky et Frederika Smetana, la compagnie
Golem Théâtre a été rapidement accueillie par des scènes françaises et
est aujourd’hui implantée dans le Trièves (Isère).

Elle s’intéresse à des thématiques en lien avec l’Histoire et la Mémoire
et mène, depuis plusieurs années, un projet, L’Europe sans bagage, en lien
avec des historiens et des lieux de Mémoire.

Golem théâtre a été accueilli par La Filature de Mulhouse, la Halle aux
Grains de Blois, le théâtre Toursky à Marseille, le théâtre des Célestins à Lyon.

Deux créations, « Héritage de feu » d’après le récit de Friedelind Wagner
et « La guerre des Salamandres » d’après Karel Capek, ont été réalisées en
coproduction avec l’Opéra de Dijon.

Le spectacle « Casablanca 41 », de Michal Laznovsky a été nominé par le
Club de la Presse du Festival d’Avignon parmi les dix meilleures créations
du OFF 2016.
