.. index::
   pair Brigitte Stora ; Rencontre avec brigitte stora pour son ouvrage 'L'antisémitisme un meutre intime (2024-06-20)


.. _stora_2024_06_20:

===================================================================================================================================
2024-06-20 **Rencontre à Grenoble avec Brigitte Stora pour son ouvrage 'L'antisémitisme un meutre intime'** |CblGrenoble|
===================================================================================================================================


Rencontre avec brigitte stora pour son ouvrage 'L'antisémitisme un meutre intime'
=====================================================================================

- :ref:`antisem:brigitte_stora`
- :ref:`antisem:meurtre_intime_stora_2024`
- http://judaisme.pluraliste.free.fr/
- https://www.cbl-grenoble.org/

Après :ref:`Que sont Mes amis devenus <antisem:stora_livre_2016>` et une
thèse doctorale en psychanalyse, l'essayiste intellectuelle et chanteuse
Brigitte Stora propose une :ref:`réflexion profonde sur l'antisémitisme <antisem:meurtre_intime_stora_2024>`

Loin d'une "œuvre de circonstance", c’est un travail de longue haleine,
mûrement réfléchi avant la séquence si brutale que nous vivons et qui vient
nous aider à prendre un peu de distance philosophique, analytique et historique,
pour mieux comprendre le retour de l'antisémitisme.

Jeudi 20 juin 2024 à 18h30
à la Maison de l'international, 1 rue Hector Berlioz, Grenoble

.. figure:: images/brigitte_stora.webp


Vidéos
=========

- https://www.youtube.com/@noamswartz7583/videos

.. toctree::
   :maxdepth: 4
   
   video-1
   video-2
   video-3
   

Maison de l'international Grenoble
=======================================


.. raw:: html

   <iframe width="600" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
   src="https://www.openstreetmap.org/export/embed.html?bbox=5.725188553333283%2C45.1914775748417%2C5.729667842388153%2C45.193027566362716&amp;layer=mapnik" style="border: 1px solid black"></iframe><br/><small>
   <a href="https://www.openstreetmap.org/#map=19/45.19225/5.72743">Afficher une carte plus grande</a></small>

::

    Maison de l'International
    1, rue Hector Berlioz
    Jardin de ville
    38000 Grenoble

    parvis des droits de l'Homme Jardin de Ville
