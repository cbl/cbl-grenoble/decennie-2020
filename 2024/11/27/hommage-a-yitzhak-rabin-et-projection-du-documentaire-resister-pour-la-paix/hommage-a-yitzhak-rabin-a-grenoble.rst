.. index::
   pair: Cérémonie de commémoration de l'assassinat de Yitzhak Rabin ; Grenoble

.. _paix_2024_11_27:

=================================================================================================================================================================================================================================================================================================
2024-11-27 **Cérémonie de commémoration de l'assassinat de Yitzhak Rabin au jardin des plantes de Grenoble**
=================================================================================================================================================================================================================================================================================================

.. figure:: images/fatym_et_yuval.webp

- https://www.youtube.com/watch?v=ukHWnSIVI34

Introduction par le maitre des cérémonies
=============================================

Mesdames les conseillères régionales, 
Mesdames la conseillère départementale, 
Monsieur le vice-président de Grenoble-Alpes-Métropole,
Mesdames et Messieurs les élus, 
Mesdames la présidente du cercle Bernard Lazare,  |CblGrenoble|
Mesdames et Messieurs les représentants et présidents d'associations,

Mesdames et Messieurs, Le 4 novembre 1995, le premier ministre israélien
Yitzhak Rabin, prix Nobel de la paix, est assassiné à Tel Aviv lors d'une
manifestation pour la paix. 

Nous sommes réunis aujourd'hui devant le tulipier planté à sa mémoire pour 
lui rendre hommage. 

.. figure:: images/plaque_tulipier.webp


0'52" Allocution de Monsieur Emmanuel Caroz, adjoint au maire de Grenoble
===============================================================================

- https://youtu.be/ukHWnSIVI34?t=52

Mesdames les conseillères régionales,
Mesdames la conseillère départementale, 
Monsieur le vice-président de Grenoble-Alpes-Métropole, 
Monsieur le suppléant d'Elisa Martin, député de l'ISERE, 
Monsieur le délégué diocésain représentant l'évêque du diocèse de Grenoble-Vienne, 
Mesdames et Messieurs les représentants et représentantes d'associations, 

Je note que les associations sont très bien représentées aujourd'hui 
et ça fait chaud au cœur. 

Mesdames et Messieurs, 

J'ai toujours pensé qu'une majorité du peuple aspirait à la paix et était 
prête à prendre des risques pour elle. 

Voilà le message d'espoir que portait Yitzhak Rabin dans le dernier
discours qu'il a prononcé le 4 novembre 1995, quelques minutes avant
que Yigal Amir ne le tue de deux balles dans le dos. 

Ce soir du 4 novembre 1995, 100 000 personnes sont rassemblées à Tel Aviv 
pour manifester en faveur de la paix. 

Lorsque Yitzhak Rabin, cet homme droit, juste, courageux, chef de guerre 
ayant abandonné son habit pour celui de la politique, est mort, c'est un 
processus de paix qui a été interrompu. 
Celui qui portait l'espoir d'un dialogue d'égal à égal entre Israël et Palestine. 

Ce dialogue d'ouverture qu'Yitzhak Rabin l'a entretenu avec Yasser Arafat, 
qui était jusqu'alors son ennemi. 

Cette discussion inédite, cette façon de faire un pas vers l'autre, a abouti aux
accords d'Oslo le 13 septembre 1993, et un prix Nobel de la paix conjoint
pour Shimon Peres, Yasser Arafat et Yitzhak Rabinn. 

Cette poignée de main n'a pas suffi à endiguer la montée des oppositions 
grandissantes dans les deux camps. 

En ce jour d'hommage, sous le tulipier, nous nous souvenons que l'assassinat 
d'Yitzhak Rabin a marqué l'arrêt du processus de paix qui semblait 
jusqu'alors le plus optimiste. 

Aujourd'hui, un an après les événements terribles du 7 octobre 2023, les 
faits semblent être le miroir de ce coup d'arrêt. 

Alors que les otages ne sont pas libérés, alors que la bande de Gaza est 
devenue une prison à ciel ouvert où meurent femmes, hommes et enfants, 
la situation est sombre. 

Nous sommes ici rassemblés. 

Nous voulons considérer les accords d'Oslo comme un précédent et la mémoire d'un
possible. 

Ce possible, en tant que ville, nous y contribuons. 

Nous embrassons la cause de la paix avec détermination et conviction, 
pour vivre dans un monde juste, solidaire, respectueux des cultures. 

C'est ainsi que nous avons choisi de mener notre politique publique à Grenoble. 

Depuis le 7 octobre dernier, nous choisissons de répondre présent à Grenoble, 
ville d'hospitalité et ville creuset, en rencontrant et soutenant 
régulièrement les associations comme aujourd'hui le Cercle Bernard Lazare 
et les autres associations. 
J'ai vu qu'il y avait également Pour un judaïsme pluaralist présent. 
Il y a également l'association des étudiants juifs de France (UEJF) , 
qui est présente aujourd'hui. 

Nous allons également recevoir les :ref:`guerrières de la paix <guerrieres:guerrieres>` tout à l'heure. 

Nous mettons chaque année également l'auditorium du musée à disposition 
pour la remise du prix Louis Blum. 

Nous dénonçons inlassablement les massacres, les victimes civiles, prenant la paix comme éternelle
boussole. 

Nous œuvrons au rapprochement de Grenoble avec les autres villes. 

Et avons rencontré récemment, la semaine dernière, au Japon, à Hiroshima,
les représentants de l'association `Nihon Hidankyo <https://fr.wikipedia.org/wiki/Nihon_Hidankyo>`_ lauréat 2024 du prix 
Nobel de la paix. Nous espérons les accueillir à Grenoble l'année prochaine. 

Lorsque notre ville s'établit comme ville refuge pour les personnes venues 
des pays en guerre, 

lorsque notre ville agit pour les transitions, pour le dialogue avec
d'autres villes à l'international, au travers d'initiatives communes, 

nous œuvrons pour la compréhension, nous œuvrons pour l'empathie. 
Ce sont les premiers pas vers la paix. 

Lorsque nous, ville compagnon de la libération, dont le maire co-préside 
cette année l'Ordre, commémorons les 80 ans de la libération de Grenoble, 
c'est à la mémoire de l'horreur des persécutions subies par les hommes, 
les femmes et les enfants juifs que nous transmettons pour qu'elles ne se 
reproduisent plus. 

Nous dénonçons et dénoncerons inlassablement l'instrumentalisation de 
l'antisémitisme par certains responsables politiques à la mémoire courte 
pour s'en prévaloir comme prétexte pour attiser la haine envers d'autres 
communautés. 

Nous, en effet, nous agissons en soutien de l'ensemble des communautés 
présentes sur notre territoire, y compris bien évidemment la communauté 
juive, au jour le jour, et pour faire que nos lieux d'espace de dialogue 
soient de véritables lieux d'échanges. 

Enfin, lorsque nous transmettons la mémoire de celles et de ceux qui se 
sont battus, qui ont résisté et qui sont morts en héros pour que
notre pays, pour que notre continent soit aujourd'hui libre en paix, c'est
au nom de cette même paix que nous agissons. 

Je vous remercie. 

6'01" |LilianeLevy|  **Allocution de Madame Liliane Lévy du Cercle Bernard Lazare** |CblGrenoble|
======================================================================================================

- https://youtu.be/ukHWnSIVI34?t=361
- https://www.cbl-grenoble.org/
- :ref:`cbl_liens:linkertree`


Bien, bonjour, 

Je salue, je ne reprends pas, je m'en excuse, tous les élus et toutes les personnalités
qui sont ici présentes, mais je remercie toutes les personnes qui se sont
déplacées, et **je voudrais surtout remercier aussi nos invités, un petit
peu nos invités d'honneur**, c'est :ref:`Fatym [Layachi, NDLR] <guerrieres:fatym_layachi>` |FatymLayachi|
qui représente ici les Guerrières de la Paix, et :ref:`Yuval [Rahamim, NDLR] <yuval_rahamim>` |YuvalRahami|
qui représente le Forum des Familles endeuillées. 

Nous espérons qu'il en sortira un peu d'espoir
---------------------------------------------------

Donc ici, ça nous donne un petit peu l'esprit dans lequel s'est fait ce 
rassemblement, et nous espérons qu'il en sortira un peu d'espoir, parce 
que, évidemment, dans ces temps sombres, c'est ce qui manque un petit peu. 
Merci. 

Alors, je remercie également la mairie qui s'est exprimée, et qui nous reçoit,
et qui effectivement nous reçoit pour cette cérémonie depuis de longues
années, cette municipalité et d'autres municipalités auparavant. 


**Et pour nous, il est très important que ce rassemblement ait lieu**
-----------------------------------------------------------------------

Et pour nous, il est très important que ce rassemblement ait lieu. 

Il nous tient à cœur de rendre hommage chaque année à :ref:`Yitzhak Rabin <yitzhak_rabin>`, comme 
il a été rappelé, victime d'un militant d'extrême droite israélien. 
Celle-là même qui est au pouvoir avec des Smotrich et des Ben Gvir. 

Le tulipier planté ici symbolise bien pour nous les occasions manquées, 
comme il a été rappelé, avec l'exacerbation de la haine attisée par le 
terrorisme meurtrier et la montée des extrémismes. 

Mais **nous voulons aussi que ce tulipier symbolise l'espoir, un espoir enraciné**, 
si je puis dire, **bien enraciné dans nos têtes et dans nos cœurs**, 
l'espoir  que les accords auxquels aspirait Rabin trouvent encore un 
terreau favorable, un **terreau fertile des deux côtés, israélien et palestinien**. 

Ce à quoi militent nos deux invités dont j'ai rappelé et dont j'ai salué 
la présence. 

**Il y a beaucoup de mouvements comme les leurs qui oeuvrent pour rapprocher les peuples**
--------------------------------------------------------------------------------------------

Il y a beaucoup de mouvements comme les leurs qui oeuvrent pour rapprocher 
les peuples et pour que le dialogue reste possible. 

Il faut avoir du courage. 

Et le film que vous allez voir, :ref:`"Résister pour la paix" <resister_2024_11_27>`, est un titre 
qui **rappelle bien que cette résistance ne va pas de soi et que le dialogue 
ne va pas de soi**. 

Donc nous voulons que ce tulipier symbolise l'espoir que les populismes 
qui donnent à tant de pays des régimes des pires reculeront. 

L'élection de Trump n'en donne peut-être pas un très bon signal. 
On verra. 

**Le gouvernement combattu par la moitié des Israéliens met en danger la démocratie**
--------------------------------------------------------------------------------------

**Le gouvernement combattu par la moitié des Israéliens met en danger la 
démocratie**, qui est la seule démocratie du coin. 

A l'intérieur, il a profondément fracturé la société et à l'extérieur, 
la paix s'est éloignée. 

**Attaqué d'abord si abominablement par le massacre terroriste du 7 octobre, 
puis par les destructions massives à Gaza et au Liban**. 

Saluons peut-être comme une petite lueur d'espoir la trêve qui vient de 
commencer. 
Espérons qu'elle ne sera pas finalement trop rapidement arrêtée et qu'elle 
sera la base d'un petit quelque chose. 

Le retour des otages, épouvantable traumatisme des familles, n'a pas été 
la priorité. 
Je cite, une déclaration du forum des otages et des familles disparues:
"Il est inconcevable et incompréhensible que près d'un an se soit écoulé 
depuis le dernier accord de leur libération. Les otages n'ont plus de 
temps à perdre". 

Des signes importants ont marqué en Israël, des signes inquiétants, pardon, 
ont marqué en Israël la commémoration de l'assassinat de Rabin. 

Je, par exemple, l'expulsion d'une députée travailliste, Naama Lazimi, 
expulsée d'une session de la Knesset en raison d'un tee-shirt qui portait 
l'inscription "**Rabin avait raison**". 

**C'est ce que nous pensons. Rabin avait raison**. 

**La solution à deux États est la seule issue possible et juste au conflit 
entre Israéliens et Palestiniens, à cet embrasement interminable qui fait 
tant de victimes des deux côtés**. 

[applaudissements]

**Allocution de Gabriel Fleury, et de Ariane Chawad, de l'Union des étudiants juifs de France** (UEJF)
====================================================================================================================

11'04"  Discours de Gabriel Fleury
-------------------------------------

- https://youtu.be/ukHWnSIVI34?t=664

::

    Ariane Chawad 


Bonjour à tous. 

Il est toujours compliqué d'évoquer la mémoire d'un homme que l'on n'a 
pas connu. 
Je suis né dix ans après l'assassinat de Yitra Rabin. 
En novembre 2005, c'était près de 200 000 personnes qui défidaient pour 
lui rendre hommage. 
Force est de constater que depuis, la mémoire de Rabin n'est de moins en 
moins honorée. 
C'est désolant, car en tant que représentant des étudiants juifs de France 
de Grenoble, témoin de la montée des violences de tous bords, je pense 
qu'il est crucial de commémorer ceux qui se sont battus contre la haine, 
pour la paix, ceux qui ont connu l'horreur de la guerre et qui ont su 
que seule la diplomatie permettra de ne pas y ressombrer. 

Alors oui, je n'ai pas connu Rabin, mais j'ai grandi avec lui, avec ses 
accomplissements, avec sa vision de l'avenir. 
Grâce à lui, lors de mes neuf années passées en Israël, j'ai pu vivre 
sereinement avec mon voisin jordanien. 

« Anima amin shilash sikoui el shalom, sikoui gadol » 
Ces mots prononcés par Rabin, deux heures avant sa mort, signifient 
« je crois qu'il y a une possibilité de paix ». 
Une grande possibilité. 

Préserver la mémoire de Rabin, c'est aussi ne jamais cesser
d'y croire, encore plus dans ces moments si durs. 

Merci. 

[applaudissements]

12'31" Discours d'Ariane Chawad
------------------------------------

- https://youtu.be/ukHWnSIVI34?t=751

Mesdames et messieurs, aujourd'hui, nous nous rassemblons pour honorer 
la mémoire de Yitzhak Rabin. 
Un homme de courage, un visionnaire, un bâtisseur de paix. 

Et pourtant, en célébrant son héritage, une pensée s'impose. 
Et si ? 

Et si Yitzhak Rabin n'avait pas été assassiné, ce soir tragique de novembre
1995 ? 

Et si son projet de paix, fragile mais audacieux, avait survécu à la haine 
et au radicalisme ? 

Peut-être vivrions-nous dans un monde où les enfants d'Israël et de Palestine 
jouent ensemble, plutôt que de grandir dans la peur. 
Un monde où la coexistence ne serait pas un rêve, mais une réalité. 

Mais l'histoire a choisi un autre chemin. 

L'assassinat de Rabin a brisé un élan, un espoir, laissant un vide que 
la violence a rempli. 

Le 7 octobre 2023, cette violence s'est manifestée de la manière la plus
brutale. 
Le massacre impitoyable de civils juifs. 
Des images insoutenables, qui rappellent les heures les plus sombres de 
notre histoire. 
Alors, en ce jour où nous nous souvenons de Yitzhak Rabin, en tant que 
vice-présidente de la section de l'UEJF de Grenoble, je veux dire ceci: 

Nous avons le devoir de poursuivre son rêve. 
Refuser la haine. 
Refuser l'extrémisme. Y compris aujourd'hui, où tout nous pousse à nous 
radicaliser. 
Travailler pour un avenir où les accords signés ne seront pas trahis 
dans le sang. 
Un avenir dans lequel l'espoir vivra. 

Merci. 

[applaudissements]

**Allocution de Fatym Layachi et de Yuval Rahami** 
=============================================================


.. _fatym_layachi_2024_11_27_rabin:

14'79"  **Allocution de Fatym Layachi en hommage à Yitzhak Rabin le 27 novembre 2024 à Grenoble** |FatymLayachi|
---------------------------------------------------------------------------------------------------------------------

- https://youtu.be/ukHWnSIVI34?t=889
- :ref:`guerrieres:fatym_layachi`

Bonjour. 

Monsieur le Adjoint au maire, 
mesdames, messieurs, les conseillers régionaux. 

C'est un honneur pour moi aujourd'hui de pouvoir saluer la mémoire de 
Yitzhak Rabin. Cet homme visionnaire comme vous l'avez rappelé dont le courage et
dont le parcours nous oblige. 

Le courage de celles et ceux qui militent en Israël et en Palestine pour 
une paix juste et durable. 

Le courage de celles et ceux qui sont endeuillés et entravés par le 
conflit et qui continuent de résister, de se battre et de militer. 

Leur courage nous oblige. 

**Je suis là aujourd'hui au nom des Guerrières de la paix** |guerrieres|
-------------------------------------------------------------------------

- :ref:`guerrieres:guerrieres`

Je suis là aujourd'hui au nom des :ref:`Guerrières de la paix <guerrieres:guerrieres>` 
Et souvent on nous dit, faire la guerre pour faire la paix c'est un oxymore
(NDLR: En rhétorique, un `oxymore ou oxymoron <https://fr.wikipedia.org/wiki/Oxymore>`_, est une figure de style 
qui vise à rapprocher deux termes (le plus souvent un nom et un adjectif) 
que leurs sens devraient éloigner, dans une formule en apparence contradictoire, 
comme « une obscure clarté » (Corneille) ou la « lumière noire » émise 
par les lampes de Wood)

Non, au contraire. 
**Nous sommes absolument convaincus que le combat pour la paix est aujourd'hui
encore plus que jamais le seul combat qui vaille**. 

Cela fait un peu plus d'un an que nous observons le Proche-Orient s'enliser 
dans l'horreur. 
Depuis ce matin du 7 octobre, nous avons ouvert les yeux sur les images 
d'un massacre innommable. 
Cela fait plus d'un an que nous appelons dans un même souffle et avec la 
même énergie à la libération immédiate et sans condition de tous
les otages ainsi qu'à un cessez-le-feu immédiat et sans condition **sur les
populations civiles à Gaza qui sont aujourd'hui massacrées**. 

Mais ce jour est aussi un jour d'espoir. 
Ce matin, un cessez-le-feu. Un accord a été trouvé alors sans doute fragile 
et peut-être même de très courte durée
Je pense que nous en sommes tous convaincus. 
Notre présence ici aujourd'hui le redit. Ça ne veut pas rien dire. 
Que cela arrive aujourd'hui, le jour où nous saluons la mémoire de Yitzhak Rabin, 
j'ai envie personnellement de rajouter un peu d'espoir à cette espérance 
que cela crée. 
Alors pour finir, oui, Yitzhak Rabin, Rabin avait raison. 
Et la paix, une paix juste et durable. 
Une paix juste et durable qui se fonde sur la légitimité pleine et entière 
des deux peuples, israéliens et palestiniens, de vivre en sécurité, chacun 
dans un État, dans un État souverain. 
Oui, cette paix juste et durable est la seule solution possible. 
Et votre présence à tous, ici, prouve et rajoute de l'espoir. 

Merci à tous. 


[Applaudissements]
 
.. figure:: images/fatym_fin_discours.webp 
 
 
17'46" **Allocution de Yuval Rahami** |YuvalRahami| 
------------------------------------------------------
 
- https://youtu.be/ukHWnSIVI34?t=1066
- :ref:`yuval_rahamim`

Bonjour et merci de nous avoir ...


je vois euh que que vous êtes très très bien au courant des situations de
notre région des situations politiques les situations sur sur le terrain
et c'est très rare de voir tellement des gens qui refusent de choisir des
côtés ; qui choisit le la paix 

donc le monde entier maintenant est divisé entre les soutiens des Israéliens 
et les soutiens des Palestiniens et nous en tant que familles endeuillées 
Israëlo-palestiniens organisation d'à peu près on était 700 avant la guerre 
maintenant on approche de 800 membres, familles qui ont perdu quelqu'un 
dans dans ce conflit

c'est très très dur à maintenir l'espoir mais on le tient toujours et 
parce qu'on sait bien qu'on a pas le choix on n'a pas le choix de de se 
laisser pour le désespoir donc on continue à diffuser dans notre société 
les Israéliens et les Palestiniens la possibilité de réconciliation 
la possibilité de la paix parce que comme mon collègue c'est on n'a pas le 
choix on n'a pas d'autres choix et même si ça va prendre encore 10 ans ou 
20 ans finalement on aura la paix on aura la dignité pour les deux peuples 
et la liberté et les sécurité donc merci de nous avoir ici soir et bon 
courage parce que c'est ce qu'on a besoin maintenant 

[Applaudissements] merci 

- https://youtu.be/ukHWnSIVI34?t=1179 (19'39")


observons un moment de recueillement 
=========================================

Nous observons un moment de recueillement. ... ... 

Merci. Mesdames et Messieurs,
la cérémonie est terminée. Je vous rappelle la projection du film 
« Résister pour la paix » qui aura lieu ce soir à 18h dans les salons 
de l'hôtel de ville. 

Bonne fin d'après-midi. ...


.. figure:: images/apres_ceremonie.webp

