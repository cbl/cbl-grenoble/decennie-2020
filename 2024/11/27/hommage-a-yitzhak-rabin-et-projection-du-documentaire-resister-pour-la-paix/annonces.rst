.. index::
   pair: Cercle Bernard Lazare; Hommage à Yitzhak Rabin 


=================================================================================================================================================================
**Annonce de l'hommage à Yitzhak Rabin et de la diffusion du film "Résister pour la paix" par le Cercle Bernard Lazare (CBL) de Grenoble** |CblGrenoble| 
=================================================================================================================================================================

Annonce par le Cercle Bernard Lazare |CblGrenoble| 
=========================================================


.. figure:: images/rabin.webp

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.733731389045716%2C45.1865192087242%2C5.737271904945374%2C45.188124156052695&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.187322/5.735502">Afficher une carte plus grande</a></small>
 
Le mercredi 27 novembre 2024 : Hommage à Yitzhak Rabin, premier ministre de 
l'état d'Israel, qui a été assassiné le 4 novembre 1995 par un fanatique 
religieux juif.


.. figure:: images/plaque_tulipier.webp


Annonces sur les réseaux sociaux
====================================

- https://kolektiva.social/@noamsw/113553462245473908
- https://kolektiva.social/@noamsw/113559618802229331
- https://kolektiva.social/@noamsw/113559687043615637
