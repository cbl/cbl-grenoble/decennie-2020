
===============================================================================================
2024-11-27 **Annexes**
===============================================================================================


Cercle Bernard Lazare de Grenoble |CblGrenoble|
=====================================================

- https://www.cbl-grenoble.org/
- :ref:`cbl_liens:linkertree`

Marc du CBL et Fatym Layachi
====================================

- https://fr.wikipedia.org/wiki/Fatym_Layachi
- :ref:`guerrieres:fatym_layachi`

.. figure:: images/marc_et_fatym.webp

   Marc du CBL et Fatym Layachi


Parents circle
======================

- https://linktr.ee/pcff
- https://parentscirclefriends.org/
- https://www.theparentscircle.org/en/homepage-en/
- https://www.progressiveisrael.org/the-parents-circle-families-forum/


.. _yuval_rahamim:

Yuval Rahamim |YuvalRahami| 
================================

- https://x.com/yuval_rahamim
- https://x.com/PCFForg
- https://www.theparentscircle.org/
- https://fr.jcall.eu/actualites/articles/youval-rahamim-reconstruire-la-confiance-avec-les-palestiniens-et-reprendre-les-negociations-de-paix

Rencontre avec Yuval Rahamim à la mairie du 3e arrdt, le 31 janvier AAAA ? de 20 h à 22 h précises – dans le cadre de l’exposition “Activistes de la Paix”, photos de Shlomo Israël
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://www.lapaixmaintenant.org/activistes-de-la-paix/

Article 2018
-------------------

- https://www.crif.org/fr/actualites/crif-trois-questions-yuval-rahamim-directeur-du-forum-des-ong-pour-la-paix

2024-11-27 **Vivian  Silver n'est pas morte** |vivian|
==========================================================

- :ref:`wwp:vivian_silver` 
- https://x.com/YonatanZeigen 
  (A father, social worker & mediator. Son of the late peace activist 
  Vivian Silver, killed Oct 7th. Following in her footsteps, making peace)
- :ref:`wwp:vivian_silver` 
- https://www.youtube.com/watch?v=iIKkQlo_XfA&ab_channel=DemocracyNow%21

Vivian  Silver n'est pas morte : chaque année , un prix sera décerné à 
deux femmes , l'une Juive et l'autre Palestinienne qui travaillent dans 
des domaines qui étaient au coeur de la vie de Vivian à savoir la promotion 
de la paix israelo arabe . 

Le premier prix a été attribué le 21/11/2024 à la politologue palestinienne 
Rula Hardal et à l'avocate israelienne May Pundak 
     

.. _yitzhak_rabin:

Yitzhak Rabin
=================

- https://fr.wikipedia.org/wiki/Yitzhak_Rabin
- https://fr.wikipedia.org/wiki/Accords_d%27Oslo

**Yitzhak Rabin** (en hébreu : יִצְחָק רַבִּין, /jitsˈχak ʁaˈbin/ `Écouter <https://fr.wikipedia.org/wiki/Fichier:He-Yitzhak_Rabin.ogg>`_), né à 
Jérusalem le 1er mars 1922 et mort assassiné à Tel Aviv le 4 novembre 1995, 
est un militaire et homme d’État israélien, Premier ministre de 1974 à 1977 
puis de 1992 à sa mort. 

En 1994, il reçoit le prix Nobel de la paix, notamment pour son rôle actif 
dans la signature l’année précédente des `accords d'Oslo <https://fr.wikipedia.org/wiki/Accords_d%27Oslo>`_.  

Le 4 novembre 1995, Yitzhak Rabin, âgé de 73 ans, est touché par deux 
balles tirées à bout portant dans son dos par **Yigal Amir, juif extrémiste 
religieux**, étudiant en droit et opposé aux accords d'Oslo. 

**Ce meurtre intervient après qu'il a prononcé un discours lors d'une 
manifestation pour la paix sur la place des rois d'Israël, à Tel Aviv**. 

Mortellement blessé, Yitzhak Rabin meurt sur la table d'opération de 
l'hôpital Ichilov de Tel Aviv quelques heures plus tard.

Il est remplacé par `Shimon Peres <https://fr.wikipedia.org/wiki/Shimon_Peres>`_ au poste de Premier ministre d'Israël 
par intérim.

**Le processus de paix israélo-palestinien a été grandement freiné à la 
suite de l'assassinat de Rabin**. 

**Ce meurtre eut également pour conséquence un élargissement de la fracture 
dans la société israélienne entre les religieux et les laïcs**. 


Liens Piolle, Brigitte Stora, Apeirogon (de Colum McCann)
===============================================================

- :ref:`piolle_2024_11_21`
- :ref:`stora_2024_06_20`
- :ref:`parents:lecture_2023_12_05`

Articles
==============

- https://www.ledauphine.com/societe/2024/11/30/une-ceremonie-en-hommage-a-yitzhak-rabin-homme-de-paix-assassine
