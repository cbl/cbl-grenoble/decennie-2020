
=================================================================================================================================================================================================================
2024-11-27 **Hommage à Yitzhak Rabin, projection du documentaire "Résister pour la paix" et débat avec Fatym Layachi des Guerrières de la paix et  Yuval Rahamim du forum des familles endeuillées** à Grenoble
=================================================================================================================================================================================================================


.. toctree::
   :maxdepth: 4
   
   annonces
   hommage-a-yitzhak-rabin-a-grenoble
   projection-du-documentaire-resister-pour-la-paix-et-debat-avec-fatym-layachi-et-yuval-rahamim
   annexes
   texte-ceremonie
   

