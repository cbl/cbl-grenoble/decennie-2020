
.. _debat_2024_11_27:

===============================================================================================================================================================================
2024-11-27 **Projection du documentaire "Résister pour la paix et débat avec Fatym Layachi des Guerrières de la paix et  Yuval Rahamim du forum des familles endeuillées**
===============================================================================================================================================================================

- :ref:`guerrieres:resister_pour_la_paix`
- :ref:`guerrieres:guerrieres`
- :ref:`raar_liens:standing_together`
- https://www.youtube.com/watch?v=VMfKKUQzO1A&ab_channel=DailyDocsandReportages


**Introduction du documentaire "Résister pour la paix"** par Emmanuel Caroz, adjoint au maire de Grenoble et Fatym Layachi |FatymLayachi| (les guerrières de la paix) 
================================================================================================================================================================================

.. youtube:: OpNOq8rKwKw

Emmanuel Caroz, adjoint au maire de Grenoble
-------------------------------------------------

Pour cette projection, résister pour la paix, résister pour la paix,
c'est le lieu, car nous sommes ici dans le salon d'honneur, vous savez sans
doute, vous le doutez tous, ce salon d'honneur a la particularité c'est
que dans la vitrine qui est derrière nous se trouve la croix de l'ordre de la
libération, et les six portraits qui nous regardent ce sont les six femmes
qui sont compagnons de la libération, qui sont résistantes, et comme vous le savez,
c'est une soirée où on parle de résistance, c'est-à-dire on peut faire
des ponts, on peut faire des ponts dans l'histoire, entre l'histoire de la
seconde guerre mondiale et ses horreurs, la Shoah, on peut faire les ponts 
avec ce qui se passe sur Israël et sur la Palestine. 
Aujourd'hui on va se faire des ponts sur la résistance. 

On a parlé un petit peu cet après-midi ; j'ai noté que le terme "résistance" 
dans les allocutions qui ont eu lieu cet après-midi, à l'occasion de la 
commémoration de l'hommage à Yitzhak Rabin, le terme résistance était 
souvent repris. 

Un autre terme qui a été repris, "l'occupation", Rabin avait raison de 
croire, il avait raison de croire en la paix, il s'est oté? la vie. 

On a parlé de la paix en 1995-2024, donc le temps passe, on n'est pas 
sur la paix, on a l'impression de s'en éloigner, donc ces soirées sont 
encore plus importantes dans la période actuelle. 

Donc nous allons pouvoir être ensemble pour cette projection de
documentaire "Résister pour la paix". 

Ensuite, merci aux guerrières de la paix, merci au Forum Israël-palestinien
des familles endeuillées d'être présents. 

Vous allez prendre la parole ensuite après le film pour un débat. 

Et le maire viendra ensuite, après le débat, pour son allocution
qui sera plus formelle que la mienne. Voilà juste un petit mot d'introduction. 

Voilà, je ne sais pas si il y a une présentation du film avant, je ne sais 
pas si vous  voulez présenter le film et ben voilà voilà ce que Edith veut
j'essaie  au maximum de répondre à ses attentes 

merci à vous toutes et à vous  tous je vous laisser la parole pour la présentation
du film. 

Ensuite, projection du film et puis débat et allocution de monsieur le maire 
pour terminer la soirée. 

Merci à tous les membres de votre présence cet après-midi, pour votre 
présence ce soir. 

Merci à vous toutes et à vous tous. 

[applaudissements]

.. _fatym_layachi_2024_11_27_intro:

2'42" |FatymLayachi| **Fatym Layachi (les guerrières de la paix) présente le documentaire "Résister pour la paix** |guerrieres|
--------------------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/OpNOq8rKwKw?t=162
- :ref:`guerrieres:fatym_layachi`
- :ref:`guerrieres:guerrieres`

Bonsoir, 

Merci à toutes et à tous de votre présence. 

Le film que vous allez voir s'appelle **"Résister pour la paix"**. 

Il a été réalisé par :ref:`Anna Hassouline <guerrieres:hanna_assouline>` et :ref:`Sonia Terrab <guerrieres:sonia_terrab>`
Et c'est une galerie de portraits de militants et de militants israéliens 
et palestiniens en Israël et dans les territoires palestiniens. 
Qui continuent pour la paix après le 7 octobre. 

En fait, l'idée de départ de ce film, le 4 octobre 2023, les guerrières 
de la paix étaient aux côtés de l'association palestinienne Woman of the Sun, 
et de l'association israélienne Woman Wage Peace pour une grande marche de femmes
qui a mis des centaines de milliers de femmes en blanc pour marcher. 

**Ça, c'était le 4 octobre 2023**
-----------------------------------

Ça, c'était le 4 octobre 2023

**Ce jour-là, les guerrières de la paix, on était convaincus que c'était possible. 
On était sûrs que ce moment était un temps fort pour l'espoir**. 

Et ce 4 octobre-là, une espérance et une espérance concrète étaient nées. 

Le 7 octobre au matin, on s'est réveillés sur cette horreur-là et... effarées. 

Et le courage de ces militants et militantes nous a encore une fois impressionnés. 
Parce que malgré les douleurs, malgré les deuils, malgré les entraves, 
malgré les difficultés, ils n'ont pas perdu l'espoir. 

C'est leurs paroles et leurs actions que ce film vous donne à voir et à entendre. 

Belle projection. 

[applaudissements]


.. _resister_2024_11_27:

**Projection du documentaire "Résister pour la paix"**, de Hannah Assouline et Sonia Terrab à 18h
=====================================================================================================

- :ref:`guerrieres:resister_pour_la_paix`

- https://www.youtube.com/watch?v=VMfKKUQzO1A

.. youtube:: VMfKKUQzO1A

Projection du documentaire "Résister pour la paix", de :ref:`Hannah Assouline <guerrieres:hanna_assouline>` 
et Sonia Terrab


**Débat** avec **Fatym Layachi** des Guerrières de la Paix |guerrieres| et **Yuval Rahamim** du forum des familles endeuillées    
=================================================================================================================================================

- :ref:`guerrieres:fatym_layachi`

Description
----------------

Débat avec une militante des :ref:`Guerrières de la Paix <guerrieres:guerrieres>`, 
`Fatym Layachi <https://fr.wikipedia.org/wiki/Fatym_Layachi>`_  
et  un responsable du `forum des familles endeuillées <https://www.progressiveisrael.org/the-parents-circle-families-forum/>`_  **Yuval Rahamim** 


- https://www.youtube.com/watch?v=j92qrPcM5-M

.. youtube:: j92qrPcM5-M

.. figure:: images/fatym_et_yuval_soir.webp

Animateur
------------------

Je me demande pourquoi, moi je suis un peu sur le sujet, c'est parler de
mes sensations en voyant ce film, c'est-à-dire pourquoi, c'est quelque
chose de très partagé, d'un côté, une sorte de réconfort immense,
à l'idée qu'il existe quelque chose comme ça, c'est tellement rare,
il s'inquiète. Et pour nous revoir un témoignage aussi important de gens
qui ont été vraiment sur le terrain, qui ont côtoyé les souffrances de
si près, et continuer immédiatement après ce qui s'est passé, continuer à
reprendre. C'est vraiment un dialogue, c'est quelque chose qui est vraiment
réconfortant. 

Et puis d'un autre côté, effectivement, ce qui est assez très douloureux 
de voir tout ça, c'est extrêmement douloureux de voir
l'énormité de chemin à parcourir un corps pour réveiller quelque chose. 

On le voit dans les moments où chacun est confronté à ses propres ennemis, 
à ses ennemis dans son propre camp. 
Et ça paraît avoir plus de difficultés. C'est difficile. 
Alors voilà, je ne sais pas qui voudrait prendre la parole pour parler 
des témoignages. 

1'29" **Yuval Rahami:  Le pacte dès le 7 octobre de continuer notre travail** |YuvalRahami| 
--------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=89
- :ref:`yuval_rahamim`


Ça va bien ? 

Ce que vous avez vu là, c'est vraiment impressionnant, mais il faut dire 
que ce sont des milliers de gens comme ça, en Israël et en Palestine, mais 
malgré tout, on est dans des minorités. 

Israël est maintenant gouverné dans un gouvernement d'extrême droite et 
je sais que vous êtes tous au courant qu'est ce qui se passe en politique 
israélienne la guerre mais nous en tant que combattant pour la paix en Israël 
et en Palestine on sent depuis très longtemps que notre combat est pas 
avec l'autre côté mais avec notre avec notre société avec notre société 
israélienne qui refuse à voir à vouloir changer la réalité, à comprendre 
qu'il y a autre solution sauf que la guerre et qui est de plus et plus 
difficile pour nous pour la paix,  activiste de continuer comme ça avec 
une société qui est qui est complètement militante et nationaliste et 
c'est pareil pour les pour notre camarade de l'autre côté palestinien ;
que eux aussi ils souffrent beaucoup des sentiments Palestiniens qui 
devienent de plus en plus séparés et découragés ; il croient pas des 
solutions qui qui viendront d'Israël et tout ça donc 

Donc on tient l'espoir parce qu'on n'a pas le choix. 
On tient l'espoir parce qu'on n'a pas le choix. 
On arrive souvent à désespérer. On va dormir dès le matin,
on commence le travail. Et le lendemain de la 7ème en octobre, moi j'étais
encore dirigeant du forum des familles endeuillées. 

C'était une organisation de 700 ou 800 familles, des 2 côtés, qui ont perdu 
quelqu'un pour la guerre. 

Et donc tous les années de ce conflit, on décidait de consacrer la vie 
à la paix. On a fait des efforts. On a fait des efforts pour la paix. 
Pour convaincre les autres camarades de notre société que la paix est 
possible malgré notre peine.

- https://youtu.be/j92qrPcM5-M?t=235 (3'55")
 
Alors on a une équipe d'Israéliens et Palestiniens, des Israéliens à 
Tel Aviv, des Palestiniens à Ubidjala????. 

Et moi et mon partenaire palestinien, on a décidé le 7ème en octobre, on 
a fait quelques heures après que la guerre commençait, on
savait déjà que ça va durer très longtemps, que ça va avoir un taux
énorme. 

Donc on a décidé de faire la conférence le lendemain, en Zoom,
de toute l'équipe d'Israéliens et Palestiniens. Et de tout parler, de tout
sortir, de savoir que n'importe qu'est-ce qui se passe, on sait exactement
ce qui se passe. 

On savait qu'il y aura la guerre. On a prévu beaucoup de gens. 
Beaucoup de gens Israéliens et Palestiniens. Et malgré tout, on va
tenir l'espoir, tenir le travail qu'on fait ensemble. 

Mais on savait qu'on ne pourra plus se rendre ensemble, soit en Israël, 
soit en Palestine, parce qu'on savait que les checkpoints vont être terminés, 
fermés. 

Et depuis là, de se rendre seulement en Zoom, en ligne. Et on a décidé de continuer
notre travail. C'était une pacte qu'on a faite cette soirée, de continuer
notre travail. 

Mais on a travaillé très très dur depuis. 

C'était très très difficile de travailler contre notre société, qui est complètement
immergée dans la guerre. De dire, il y a d'autres possibilités, il y a
d'autres solutions. 
Il ne faut pas croire que la guerre va se résoudre va se résoudre quoi
que ce soit.

- https://youtu.be/j92qrPcM5-M?t=341 (5'41")

Merci. 


.. _fatym_layachi_polarisationç2024_11_27:

5'40 **Fatym Layachi : il faut lutter contre les débats polarisés**  |FatymLayachi| 
----------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=340


Merci Yuval.
Merci Yuval pour ces mots et ce courage et cette force. 

Dès le 7 octobre, de se dire non ; le dialogue, il ne faut pas le rompre. 
Et ça, c'est admirable. 

Et vous, enfin moi et je pense qu'il y a beaucoup de gens dans cette salle,
ce conflit, il nous passionne, il nous inquiète, il nous déchire,il nous émeut. 
Mais il faut se souvenir qu'on ne le vit pas. 
Du moins, nos quotidiens ne sont pas entravés par ce conflit. 
Nos déplacements, notre vie quotidienne n'est pas entravée. 

6'20" **certains d'entre nous ne connaissent même pas les gens sur place, on arrive à polariser le débat, à transformer des combats qui sont justes pour la paix en débats extrêmement polarisés, violents, avec toujours  cette injonction à choisir un camp identitaire, avec cette notion d'empathie à géométrie variable**
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=380

**Et quand on voit qu'à des milliers de kilomètres, alors que souvent, 
certains d'entre nous ne connaissent même pas les gens sur place, on 
arrive à polariser le débat, à transformer des combats qui sont justes 
pour la paix en débats extrêmement polarisés, violents, avec toujours 
cette injonction à choisir un camp,  identitaire, avec cette notion 
d'empathie à géographie [géométrie, NDLR] variable**. 

**Ça, je pense qu'il faut lutter contre ça**, parce que.. et leur courage 
à celles et ceux qui sur place, vivent cette guerre, et la vivent dans 
leur chair, dans leur deuil, dans leur incapacité de mouvement pour certains. 

**Leur courage nous oblige**. 

On n'a pas le droit, nous deux, de se laisser entraîner par des fausses 
passions, de se prendre pour des faux justiciers, et de vouloir, au nom 
de je ne sais quelle idéologie, sauver l'homme, refuser d'entendre 
la douleur de l'autre. 

Je suis convaincue que nos indignations, elles ne s'opposent pas, au contraire 
elle s'additionnent. 
Et si, le 7 octobre au matin, on n'a pas été horrifié par ce massacre, 
qu'il faut nommer, qui est un massacre terroriste, mené par des monstres 
sanguinaires, si le matin du 7 octobre, on n'a pas été horrifié, dans notre chair, 
**c'est notre humanité qui fait faillite, parce qu'aucune cause, même la plus légitime,
ne mérite de défendre la barbarie**. 

Et si le 8 octobre, au moment où la riposte israélienne a commencé, et qu'on
sentait le massacre qui s'annonçait, et encore, je pense qu'on n'imaginait
pas le 8 octobre, le massacre, le drame humanitaire, la famine, et encore,
on a quand même très peu d'images de Gaza, donc je pense que tout ce qu'on
imaginait, tant de ça n'a pas été. 

Si à partir du 8 octobre, on n'a pas été indignés, révoltés, si les rares 
images qui nous parviennent de Gaza aujourd'hui, ne nous... ne nous révoltent 
pas, ne nous indignent pas, ne nous blessent pas, **là encore, c'est notre 
humanité qui est en faillite**. 

8'42" **Une vie est une vie**
++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=522
- https://en.wikipedia.org/wiki/Kfar_Aza

**Une vie est une vie**. 

À Gaza, comme à `Kfar Aza <https://en.wikipedia.org/wiki/Kfar_Aza>`_. 
À Kfar Aza, comme à `Gaza <https://fr.wikipedia.org/wiki/Gaza>`_

Et à partir du moment où on refuse à l'autre la souffrance, à partir du moment
où on refuse à l'autre de lui accorder notre empathie, **c'est qu'on lui
refuse son humanité**. 

Et comment osons-nous, à des milliers de kilomètres, confortablement assis, 
parce qu'on a la chance de vivre en démocratie, on a la chance de vivre 
dans un pays démocratique, égalitaire, évidemment plein d'imperfections, 
mais ça reste une démocratie, et des institutions démocratiques. 

Nous vivons en Europe, et alors oui, l'Union Européenne aussi, elle est 
imparfaite, mais toujours est-il qu'elle s'est construite avec deux
États qui se sont livrés à une guerre fratricide, et qui ont su faire
la paix, et se dire plus jamais ça, et oui, **plus jamais ça**, il n'y aura
plus jamais de guerre entre la France et l'Allemagne. 

- https://youtu.be/j92qrPcM5-M?t=594 (9'54")

[applaudissements]

9'56" **Cette passion ne peut pas être un prétexte pour se laisser aller à ces indignations sélectives, à des vociférations**
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=596

Tout ça veut dire qu'à nous, évidemment que ce conflit nous passionne,
mais **cette passion ne peut pas être un prétexte pour se laisser aller à ces
indignations sélectives, à des vociférations**. 

Et... Et quand, en fait, il n'y a pas de pro-Palestinien, de pro-Israël, 
ce n'est pas un match de foot, c'est... il n'y a pas de... 

Là, pour l'instant, il n'y a pas de gagnant, il n'y a que des perdants. 

**Il y a des familles d'otages, dont on parle de moins en moins, il y a des 
otages dont on ne sait rien**, et il y a des milliers de familles palestiniennes 
à Gaza, mais aussi en Cisjordanie, dont on parle malheureusement moins 
depuis un an. 

**Mais toutes ces vies se valent**. 

**Et toutes ces vies méritent de vivre en sécurité, en paix, en dignité**. 

10'55" **Et ces deux peuples sont pleinement légitimes**
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


- https://youtu.be/j92qrPcM5-M?t=655

Et ces deux peuples sont pleinement légitimes, et **ces légitimités pleines et
entières**, j'insiste là-dessus, ce n'est pas juste une légitimité de fait,
ces deux peuples sont légitimes, ces deux peuples méritent une paix juste et
durable dans deux Etats, autonomes et souverains. 

Et je pense que vraiment, ayant l'humilité d'écouter ces voix-là, qui 
arrivent à braver non pas le chagrin, non pas la tristesse, parce que si 
vous écoutez les témoignages du cercle des familles endeuillées, 
ce sont des deuils impossibles.
 
Et non, ils ne dépassent pas chacun. 

En revanche, ils vont au-delà de la colère, au-delà du ressentiment. 

Et ça, c'est admirable. 

Alors nous, on ne peut pas. On leur doit ça. C'est le minimum qu'on leur doit. 
De respecter ce combat, admirable qu'on aura fait si juste. 

Et leur courage nous oblige toutes et tous. 


.. _histoire_autre_2024_11_27:

11'55" **Je vous invite à lire un livre qui a été réédité il y a quelques mois, qui est extraordinaire, qui s'appelle "Histoire de l'Autre"**
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=715
- https://www.lianalevi.fr/catalogue/histoire-de-lautre/
- :ref:`antisem:histoire_de_lautre_2024`

.. figure:: images/histoire_de_l_autre.webp

   :ref:`antisem:histoire_de_lautre_2024`

Et je voudrais juste dire une petite chose. 

Je vous invite à lire un livre qui a été réédité il y a quelques moisn 
qui est extraordinaire, qui s'appelle **"Histoire de l'Autre"**, qui est 
préfacé par Elie Barnavi et qui, à la base, ce projet a été mené une 
vingtaine d'années, ou un peu plus, par sept historiens palestiniens et 
sept historiens israéliens [six historiens palestiniens et  six historiens israéliens, NDLR]. 

Et l'idée, c'était juste de coucher par écrit prendre un cas, lire un livre, 
et à chaque fois travailler dans le moment et le temps, et de voir ce 
qu'il y a de plus en plus de ce que l'on peut faire. 

[ inaudible]

- https://youtu.be/j92qrPcM5-M?t=758 (12'38")

Et je pense qu'en cette époque où jamais les débats n'ont été aussi polarisés, 
jamais on a été autant sommé de choisir un camp et surtout de bien choisir 
le camp auquel on est censé appartenir, en fonction de notre couleur de 
peau, de notre lieu de naissance, de la manière dont on prierait. 

12'54" **Et on est sommés de choisir un camp, et surtout de ne pas écouter l'Autre**
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=774


**Et on est sommés de choisir un camp, et surtout de ne pas écouter l'Autre.**

Alors que non, si on veut la paix, je pense que c'est ça, la haine, ne peut
pas être ni un projet de société ni un projet d'avenir. 

Et la paix, c'est pas un vœu pieux, c'est tout ce que la notre réalité, 
vraiment c'est tout ce que la notre réalité. 

Je pense qu'il me le prouve suffisamment, c'est le choix le plus courageux,
le plus audacieux, c'est pas juste une histoire de bon sentiment, au moins
là. Yitzhak Rabin, à la base, n'est pas particulièrement copain avec les
Palestiniens, Amir Haïrlan?? non plus, Sarah Hume??, c'est avec elle qu'on lutte,

**on fait la paix avec ses ennemis**. 

Et la paix, c'est pas juste un vœu pieux, c'est une solution pragmatique, 
politique, réaliste, et c'est ce que méritent aujourd'hui ces deux peuples, 
c'est ce que méritent ces enfants. 

Et c'est revenir aux faits, je pense que ça fait du bien, et ça permet de se dire,
derrière une passion exacerbée, c'est normal, je pense que ce conflit nous touche toutes
et tous quelque part, et déchaine des passions, on souvent dit quelque chose
comme « peut-être que, ok, il y a la passion, revenons-en juste aux faits,
essayons de les comprendre, et surtout d'écouter l'autre, d'écouter ses
souffrances. 

Et il me semble en tout cas que c'est... parce qu'évidemment on a tous 
des biais, et on aborde le conflit avec des biais, mais je pense que 
**le seul biais qui vaille c'est le biais habituel et historique, qui reconnaît
une légitimité pleine et entière de ces deux peuples, à ces deux peuples
qui vivent sur cette terre**. 


14'40" Yuval Rahami : **Chaque Israélien, chaque Palestinien est devenu un peu plus Israélien, ou un peu plus Palestinien**
--------------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=880
- :ref:`yuval_rahamim`


Je veux dire un mot sur la neutralité qu'on parlait, qui était personnelle, 
parce que, dès le début de cette guerre, on a été tous transformés 

à notre côté, au moins en notre, parce que la... les journées qui venaient 
après le 7 octobre ces trottos, on a... ça dévoilait l'horreur qui s'est 
passée, et... ça nous touche personnellement.

Donc on peut parler... actuellement, on peut être intellectuel sur les conflits,
mais on est quand même fait à partir de ça, parce que notre... notre
ami était là, il s'est fait tuer et **donc chacun d'Israéliens, de Palestiniens, 
est devenu un peu plus Israélien, ou un peu plus Palestinien.** 

Donc le conflit est personnel. 

En moi-même, j'ai vécu mon Israélité, en même temps, mon volonté
de protéger les Palestiniens, mes camarades, mes employés, et... pour eux,
c'était le même. 

Alors chacun d'entre vous, je pense que le 7 octobre, le 8 octobre, 
il y a eu des sentiments très très forts, très très forts, concernant 
notre nationalité, notre peuple, et la douleur, et la souffrance,
et l'horreur, 

16'08" Et en même temps il faut comprendre que ces conflits ne nous laissent pas oublier notre... notre but, notre engagement
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=968

Et en même temps, il faut comprendre que ces conflits ne nous laissent 
pas oublier notre destin... notre but, notre engagement de changer cette réalité, 

**de ne pas accepter de ne pas accepter la haine, de ne pas accepter
la vengeance**, et de... continuer notre... notre combat, c'est très très
difficile, et... 

et j'espère bien que vous comprenez ça, et ne pas prendre
de côté l'horreur qui nous a blessé. 

16'55" Animateur
-------------------------

- https://youtu.be/j92qrPcM5-M?t=1015

Justement, ce que vous
venez de dire, depuis le 7 octobre, donc il s'est passé un an, un plus
d'un an, et... donc vous parlez de votre réaction immédiate qui vous a
transformé, enfin, cet événement qui vous a transformé, je me demandais
d'abord comment ça s'est passé tout de suite après, entre vous ? 

Est-ce qu'il y a eu une sorte de flottement brusquement, ou l'émotion enfin, qui est
venu dessus, et a fait qu'il y ait eu une sorte de déloignement, ou entamé,
quelque chose comme ça, parce que bon, maintenant, il y a déjà un certain
recul malgré le fait que la France a évidemment s'est perpétuée, mais
face à la... à l'intensité de ce qui s'était passé à ce moment-là,
euh... bon, il y a eu, de toute façon, une sidération même de tout le
courant en Israël qui est contesté, le gouvernement, etc. 

Pendant un temps, enfin, il y a eu beaucoup de temps pour que ça se 
remette en route, tout ça, hein. 

Est-ce que vous avez vécu les mêmes sidérations ? 


17''59" Yuval Rahami: **chacun était exposé à des images complètement différentes** 
--------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=1079

Et oui, comme j'avais dit, c'était... nous, on était... déjà
coincé dans un certain point personnel, avec la perte et la paix et la
douleur, en même temps, on était obligé de continuer de faire contre
notre... partenaire palestinien, mais aussi et chacun était exposé à des
images complètement différentes. 

Nous, en tant qu'Israéliens, on a vu la télé, on a regardé des actualités, 
et on a vu des images de... ce qui s'appelle le village Kfar Aza, 
le village de Kfar Aza, et la destruction qui était... qui se passait là-bas, 
et la... la partie de Nova du côté palestinien, et chacun connaît le cas 
qui était perdu, donc, qu'est-ce qu'on peut faire ? 

On est Israéliens, on a les émotions qui sont... 

en même temps, on a les Palestiniens d'autres équipes, qui travaillent, 
qui continuent à travailler, et qui voient les images qui arrivent de Gaza, 
donc on a passé un an très très difficile, on a beaucoup investi dans 
la préservation de communication entre... dans l'équipe, parce que, comme 
j'ai dit avant, c'est le sentiment qui est très très fort qui lève 

et... on a gardé ça avec beaucoup d'aide de... de... d'autres associations, 
pour... pour maintenir la possibilité des communiqués entre Israéliens et Palestiniens,
qui c'était très très rare dans les groupes, je pense qu'on était dans 
les seules oraganisations qui arrivaient à conserver la communication
ouverte entre les... dans l'équipe. 

Et... c'était très délicat, c'était très... tranquille, mais quand même 
on a... on en est arrivé, et... on a même pris toute l'équipe, parce 
qu'on... il n'y avait pas de possibilité de... de... de se rendre ensemble, 
soit en Palestine, soit en Israël, 

20'04" on a pris toute l'équipe en Chypre,  pour un... pour un séminaire de quelques jours
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=1204

On a pris toute l'équipe en Chypre,  pour un... pour un séminaire de 
quelques jours, pour qu'on... on peut se mettre ensemble pour quelques journées,
pour pouvoir... pour regarder dans les yeux, pour... pour... pour s'asseoir
dans un cercle et parler de tout... de tout ce qui est boulot??, de tout ce
qui est... de tout ce qui est... de toute chose qui... qui n'est... qui ne
termine pas, qui se continue, donc... 

il faut toujours soutenir ça, il faut toujours soutenir... la sensation 
d'équipe, qui travaille ensemble dans une réalité qui est presque 
impossible, oui, c'est presque... comme c'est difficile pour nous, entre... 
dans notre société israélienne, et... j'avais dit, pour les palestiniens, 
c'est des ???... c'est des pierres, quelquefois,
vraiment dangereux, de... de... de déclarer qu'ils travaillent avec des 
Israéliens pour la paix,  parce que la plupart des sociétés palestiniens 
ils vivent dans le  ?? 


21'15" **Fatym Layachi sur ses amis à Ashdod, Tel-Aviv, le 13 novembre 2015 à Paris** |FatymLayachi|
-------------------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=1275
- https://fr.wikipedia.org/wiki/Ashdod
- https://fr.wikipedia.org/wiki/Tel_Aviv-Jaffa

c'est un peu comme je te le disais, pour nous, à des milliers de kilomètres, 
c'était différent, évidemment que nous, non, au sein de nos militantes, 
non, il n'y a pas eu de... 

il n'y a pas eu de froid, au contraire, je  crois qu'on s'est jamais sentis 
plus ensemble qu'à ce moment-là, et 
surtout, je ne me souviens pas que le 7 octobre, on avait cette sidération, 
cet effroi, et les premières images, on ne prenait pas, on a mis du temps, 
à comprendre l'ampleur, et cette sidération, cet effroi qui grandissait, et après,
comme à chaque fois, malheureusement, qu'un point de la planète est 
ensanglanté, c'est un coin où on a des amis, on essaie de contacter
nos potes, moi, j'ai des amis qui vivent à `Ashdod <https://fr.wikipedia.org/wiki/Ashdod>`_, 
j'ai essayé de joindre, je parvenais pas à joindre pendant plusieurs heures, 
j'ai des copains de `Tel Aviv <https://fr.wikipedia.org/wiki/Tel_Aviv-Jaffa>`_ 
qui, potentiellement, étaient à ce festival, mais voilà, 

comme il y a un attentat, aujourd'hui, où il y a des gens qu'on
connait, et après, dès le soir même, en tout cas, quelques heures plus
tard, j'avais mes meilleurs amis palestiniens, dont la sœur est marié,
à un Gazaoui et qui vit à Gaza depuis trois ans, et voilà, elle est restée
dans un tunnel de plusieurs jours parce qu'on n'arrivait pas à la joindre,
et aucune avec aucune idée de savoir si elle était en vie morte euh voilà
et ça c'est toutes ces inquiétudes là mais qui sont qui sont les mêmes
qui sont les mêmes inquiétudes avec ... ce drame à Gaza qui dure et dont 
on voit pas la fin ;

22'49" **Sur le cessez le feu du mercredi matin 27 novembre 2024**
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=1369

On ne peut qu'espérer  que ce cessez le feu qui a été signé ce matin (du mercredi 27 novembre NDLR),
cet acccord qui a été signé ce matin soit le début du commencement; un début 
d'espoir je sais pas 

mais mais non pareil j'entends très bien quand Yuval il dit que que le 7 octobre il s'est
jamais senti aussi israélien et en même temps qu' n'a jamais eu autant
envie de protéger ses partenaires palestiniens 

23'14" Sur les attentats du 13 novembre 2015 à Paris
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=1394
- https://fr.wikipedia.org/wiki/Attentats_du_13_novembre_2015_en_France

Souvenez-vous l'état dans lequel on était;  en tout cas moi qui suis parisienne 
le 13 novembre (2015, NDLR) par exemple 

moi je crois que je me suis jamais  senti autant autant parisienne que 
ce jour-là parce que c'est nous c'est  c'était nous dans ce qu'on était, 
dans dans enfin moi je devais aller à  un concert ce soir là moi j'ai 
des amis qui dînaient sur une terrasse qui a été ensanglanté ; 
dans sa table il sont dix à être morts et je pense qu'on on était on s'est
senti uni et fort parce qu'on savait que c'était à nos valeurs communes à
notre identité, à notre socle commun qu'on s'attaquait aussi ce soir là 


23'52" Les monstres sont barbares mais le terrorisme ne tue pas à l'aveugle en fait en tuant sur les terrasses et et et au Bataclan c'était aussi nos symboles
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=1432
- https://fr.wikipedia.org/wiki/Bataclan

Les monstres sont barbares **mais le terrorisme ne tue pas à l'aveugle en fait
en tuant sur les terrasses et au `Bataclan <https://fr.wikipedia.org/wiki/Bataclan>`_ c'était aussi nos symboles**

Je comprends très bien ce que je dis quand tu dis que ça il y a quelque
chose d'israélité qui s'est affirmé le 7 octobre et que ça allait de
paire avec cette ce besoin viscéral aussi de continuer à protéger et
à être ; et c'est c'est merveilleux que vous ayez pu continuer ce dialogue
et vraiment et là encore c'est c'est une preuve s'il en fallait encore une
qu'on ne peut ; qu'on doit qu'on vous doit euh qu'on vous doit cette nuance
**qu'on vous doit de ne pas sombrer dans la polarisation du débat** 

on on vous voit ça vraiment merci Yuval

24'43" **Yuval sur conférence pour la paix en juillet 2024  qui a regroupé 6000 personnes**
--------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=1483

Je vais dire quelque chose sur le côté optimiste 

il y avait un petit morceau euh la conférence qui est qui qui a eu
lieu à Tel aviv en juillet c'était une conférence pour la paix pour la
première fois qui joignait 6000 personnes qui ont payé un billet pour y
aller à Tel Aviv  et au début on croyait que c'était pas possible 

les gens ils vont pas tenir parce que tout le monde était dans la guerre 
et par contre dans la préparation de cet événement je commencais à voir 
des nouveaux visages des nouveaux personnes personnes qui ont qui n'ont pas
l'habitude;  qui n'avaient pas l'habitude de partager, de s'engager dans
les activités de la paix 

j'ai senti qu'il y a chose à se passait parce qu'il y a beaucoup de gens 
qui sont activistes pour des tas de choses, tas de causes et dans ce
moment-là après après 7 octobre ils ont compris que c'est bien de lutter
pour le droit des homosexuels ou le droit des femmes, droit des enfants le
droit de ou n'importe 

mais la démocratie mais en Israël il y a une chose qui change tout 
qui a l'influence de tout ce qui se passe en Israël c'est la paix c'est 
l'absence de la paix 

Sans avoir la paix entre Israeliens et Palestiniens Israël n'a pas de futur
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

donc il y en a il y a des gens qui qui s'éloignaient de cette tâche 
activiste de la paix qui était mauvaise jusqu'à ce jour-là et on décidait 
et on comprit que **sans avoir la paix entre Israeliens et Palestiniens 
Israël n'a pas de futur**

[applaudissements]

et que les combats pour l'avenir d'Israël et pour l'existence
d'Israël parce qu'Israël sans paix la société Israël va être détruite
complètement 


donc ça c'est le côté un peu optimiste je pense qu'il y a plus de gens 
maintenant qui comprennent **que la paix est est une nécessité existentielle 
pour Israël** 

[applaudissements]

- https://youtu.be/j92qrPcM5-M?t=1628 (27'08")

27'08" Fatym Layachi |FatymLayachi|
------------------------------------------------


pareil pour faire écho de Yuval au propos du nous
aussi aux guerrières on a vu les rang nos manifestations grandir et 
au début on était quelques centaines et avec une une résonance qui
existait médiatiquement mais qui était assez niche on va dire et au fil
des mois maintenant dans des manifestations qu'on organise attire
des milliers de gens 

quand on participe à des manifestations comme samedi dernier lors de la Marche 
contre les violences sexuelles notre ; on a fait un cortège de guerrières 
de la paix où on était des milliers ce qui aurait été impossible ce qui 
était inimaginable comme ce que tu disais 

il y a il y a quelques temps en septembre on a organisé un très gros événement
au Théâtre National de la Colline à Paris où beaucoup d'activistes
Israéliens et Palestiniens dont certains que vous avez vu dans le film sont
venus prendre la parole et pareil il y avait 1000 personnes dans la salle
et 10 000 personnes qui étaient connectés 
on a fait une le théâtre a fait une retransmission en direct sur des 
canaux de diffusion il y avait 10000 personnes c'est pas anodin

28'23" « Women Wage Peace » et « Women of the Sun » finalistes du Prix Sakharov 2024
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=1703
- https://www.europarl.europa.eu/topics/fr/article/20240913STO23909/le-prix-sakharov-2024-est-decerne-aux-dirigeants-de-l-opposition-venezuelienne
- https://en.wikipedia.org/wiki/Women_of_the_Sun_(movement)
- https://www.fondation-raja-marcovici.com/en/uncategorized/interview-of-yael-admi-founder-of-women-wage-peace-and-reem-hjajara-founder-of-women-of-the-sun.html
- https://fr.wikipedia.org/wiki/Prix_Sakharov

Là Reem Al-Hajajreh que vous avez vu dans le film qui a fondé l'ONG woman of the sun
et  woman wage peace l'organisation Israéliennes étaient finalistes du Prix Sakharov 
et la semaine prochaine elles sont reçues au Parlement européen et ça 
non plus c'est pas rien : une projection du film au Parlement européen 
et donc on est donc 

on se dit oui on est au bon endroit et que cette ligne il faut continuer de
la défendre et de la porter et que on entend, on a plein de gens qui viennent
voir  que c'est un espace de réparation et de de réconciliation où
ils se retrouvent 

29'07" **Oui il est possible aujourd'hui de condamner dans un même souffle et sans ambiguité toutes les horreurs qui sont commises**
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=1747

et que  oui il est possible aujourd'hui de condamner dans un même souffle 
et sans ambiguité toutes les horreurs qui sont commises ;

toutes les horreurs que la guerre excusez-moi 

il est possible d'appeler dans un même souffle et avec la même conviction 
à la libération de tous les otages d'un cessez le feu à Gaza et que pour 
nous c'est pas antinomique au contraire et j'ai l'impression que qu'on est 
de plus en plus nombreuses et nombreux à à porter ce message en tout cas 
nous on voit dans on a de plus en plus d'adhérents on a plus on est de plus 
en plus médiatisé et surtout chaque chacune de nos manifestations 
maintenant on est on est des milliers ... ce qui est ce qui fait du bien 
quelque fois 


- https://youtu.be/j92qrPcM5-M?t=1789 (29'49")

des questions de la salle des interventions ? 
qui voudrait prendre la parole ? 

30'18" **Jean-Paul Vienne du Comité de l'Isère de Mouvement de la Paix**
-------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=1818
- https://fr.wikipedia.org/wiki/Mouvement_de_la_paix
- https://www.mvtpaix.org/wordpress/

bon je vais peut-être me passer du micro à moins que vous vouliez 
enregistrer ça bon 

Jean Paul Vienne du Mouvement de la Paix 

Je rentre je tout droit de de Paris où j'ai assisté notre conseil national 
et vous il a beaucoup été question naturellement mais pas seulement du 
Moyen-Orient et du proche Orient 

il y a le proche orient qui où il y a des conflits sanglants dans ce monde 
et 

Il existe une rue "Yitzhak Rabin" à  Saint-Ouen-sur-Seine (93400)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://fr.wikipedia.org/wiki/Saint-Ouen-sur-Seine

alors d'abord une petite toute petite chose pour l'anecdote j'ai découvert 
en réalité ça s'est passé à Saint-Ouen où nous avons notre siège et j'ai 
découvert que notre rue où il y avait le siège [rue Dulcie September, NDLR] 
était prolongée par une  autre rue à qui s'appelle **rue Yitzhak Rabin** 
je savais pas qu'il y avait  des rues Yitzhak Rabin en France il y en a 
une à 20 mètres près  du siège du mouvement de la paix son siège 
[9 Rue Dulcie September, 93400 Saint-Ouen-sur-Seine, NDLR]

.. raw:: html

   <iframe width="800" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" sandbox="allow-forms allow-scripts allow-same-origin" src="https://www.geoportail.gouv.fr/embed/visu.html?c=2.3204297556255122,48.904931834414214&z=18&l0=GEOGRAPHICALGRIDSYSTEMS.MAPS.3D::GEOPORTAIL:OGC:WMTS==aggregate(1)&l1=ORTHOIMAGERY.ORTHOPHOTOS::GEOPORTAIL:OGC:WMTS(1)&l2=GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2::GEOPORTAIL:OGC:WMTS(1)&permalink=yes" allowfullscreen>
   </iframe>
   
.. figure:: images/rue_rabin_saint_ouen.webp


.. _vivian_silver_2024_11_27_vienne:

31'00" **Nous avons d'abord évoqué la la mémoire de de Vivian Silver** |vivian| + Les guerrières de la paix Hanna Assouline & Sonia Terrab 
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=1860
- :ref:`wwp:vivian_silver`

bon plus sérieusement nous avons d'abord évoqué la mémoire de de Vivian
Silver ..  que nous avons nous avions eu l'honneur de recevoir à notre siège il
y a 3 4 ans je crois qui avait eté pas par moi naturellement c'est nos 
camarades du 14 et 15e arrondissement de Paris je crois qui l'ont accueilli 

alors il est question de même de lui donner de donner son nom à une de nos salle s 
le problème c'est qu'on n'a par pas tellement de pièces ; c'est qu'elles 
ont déjà toutes des noms de femmes valeureuses alors on va voir ce qu'on 
peut faire pour l'honorer 

alors cela dit on nous a projeté un film ; un film avec là où étaient 
interviewé les deux réalisatrices de ce film Hanna et Sofia [Sonia, NDLR] 
et avec des extraits de ce film d'ailleurs et qui ont parlé longuement 
de l'engagement de des guerrières de la paix et du Forum israëlo-palestinien 
des familles endeuillés 

et et puis après nous les avons eu en en liaison comment s'appelle internet 
visio-connférence nous les avons eu à peu près pendant une heure et qui 
nous ont expliqué leur travail militant au sein de ces deux associations 
et je voudrais terminer en  parlant d'une autre association dont il n'a 
pas été question soir mais nous eu un compte-rendu hein et une association 
qui de notre point de vue est également très très méritante qui s'appelle 
en France c'est ... "les amis français de névé Shalom Wat al salam" qui veut 
dire lois?? de la paix et qui font et que c'est un village en  Israël qui 
est centré autour d'une école de la paix et ils ont des donc des correspondants 
en France et ils ont expliqué tout le travail militant de cette association 
je dirais d'amitié et de travail militant pour la paix en Israël même 

voilà 

donc ça on va dire que tout ce conflit nous a beaucoup occupé


- https://youtu.be/j92qrPcM5-M?t=2000 (33'20")

[applaudissements]

Emmanuel Carroz : et donc vous n’avez pas de question
------------------------------------------------------------

et donc vous n'avez pas de question 

33'37 Une femme : **il y a effectivement un abime  pour cette réconciliation entre les Palestiniens et les Israéliens aussi entre Israéliens question: comment vous voyez ça et comment ça peut avancer ?**
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=2017

Moi je me pose des questions alors oui bien sûr ce documentaire est remarquable 
on est bien d'accord on voudrait surtout dire effectivement très fort de 
notre ; aussi loin qu'on est ; mais je pose une question par rapport 
aux Israéliens je veux dire je pense qu'il y a effectivement un abime 
pour cette réconciliation entre les Palestiniens et les Israéliens mais 
comment est-ce que vous voyez, vous, je pense qu'il y a un abîme énorme 
aussi entre Israéliens enfin c'est ce que je ressens  de ce que je peux lire
aussi hein entre voilà vous êtes quand même très très déchirés 

voilà 

comment vous voyez ça et comment ça peut avancer ?

34''28" Un homme : **on a parlé  dans le film solution à deux Etats,  est-ce-que c'est encore réalisable, espérable ?** 
------------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=2068

Je voudrais juste rajouter sur la question une question très simple mais 
la réponse vous appartient quant à sa complexité: est-ce que, on a parlé 
dans le film solution à deux Etats,  est-ce que c'est encore
réalisable espérable ?

34'54" Yuval Rahami: **il y a plein de solutions tous sont valables ce qui manque c'est la volonté**
-----------------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=2094

ah vous attendez des réponses ? 

C'est des questions très très simples 

au niveau de je commence par la deuxème question; au niveau de la solution 
il y en a plein de solutions sur la table il y a un Etat, deux Etats 
un Etat de patrie il y a plein de solutions tous sont valables ce qui 
manque c'est la volonté 

qui s'il y a des volonté de la part du gouvernement israélien et de la 
leadership palestinienne; s'il y a la volonté s'il y a des partenaires 
on arrive à une solution 

c'est n'importe ça ça m'intéresse pas si ça deux Etats ou un Etat la 
solution doit être respecter, les Droit de l'homme, les droits de dignité,
des liberté tout ça

mais tant qu'il y a pas de volonté peut m'importe quelle solution on l'annule
parce qu'on annule les toutes les solutions 

ok 

une fois il y a la volonté et et les soutiens de la communité internationale 
bien sûr on aura parce que les solutions qui sont sur la table sont presque 
finies presque presque complet donc il faut s'asseoir un mois, 2 mois, 3 mois 
et on résoud toutes les toutes les problèmes même les deux Etats hein 
même avec les changements des frontières tout ça 

36'23" C'est la volonté qui n'existe pas
++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=2183

donc il y a des solutions  c'est pas une question de solutions qui n'est 
pas valable c'est la volonté qui n'existe pas. 

et pour la deuxième question la première question sur la société Israëlienne 
c'est vrai c'est vrai qu'il est très très déchiré maintenant 

euh j'ai pas de réponse j'ai pas de réponse on sait bien qu'il y a l'influence 
très très forte de de Netanyahou sur la société pendant 20 ans déjà donc 
c'est très très difficile à renverser ...

37'04" il y a aussi la démographie qui  change
+++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=2224

et il y a aussi la démographie qui change en Israël vous savez bien que 
les orthodoxes sont les orthodoxes et les nationalistes religieux sont 
ça augmente donc je peux pas vous donner de réponses; on a l'espoir
on a l'esprit qui monte maintenant avec toutes les manifestations
donc on a le sentiment que la majorité qui était dormante tout les dans
ces dernières années se réveille maintenant pour pour combattre pour
pour l'Israël progressif et libéral tout ça 

qu'est-ce que ça va donner ? je sais pas.

- https://youtu.be/j92qrPcM5-M?t=2266 (37'46")


Animateur
    Il y a encore pas mal de questions 


37'49" Fatym Layachi |FatymLayachi|  **il y a une carence  de leadership et qui est aussi un obstacle un obstacle à la paix**
-------------------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=2269

https://youtu.be/j92qrPcM5-M?t=2275

Effectivement il y a cette question il y a cette question du  leadership 
donc d'un côté il y a un gouvernement d'extrême droite suprémaciste, maximaliste 
qui ne veut pas la paix et de l'autre côté il y a il y a une carence 
de leadership et qui est aussi un obstacle un obstacle à la paix et 
et il faut écouter ces voix de la société civile palestinienne parce 
qu'elles existent ; elles sont de plus en plus nombreuses vous avez 
entendu certaines mais il y en a d'autres qui qui sont extrêmement 
pragmatiques ; beaucoup de femmes militantes 

et par comme dis justement Yuval la solution, les solutions elles existent 
en revanche il faut vouloir les appliquer 

et parce que il y a pas de  solution magique effectivement les frontières 
de 67 aujourd'hui c'est un peu compliqué il faut s'asseoir autour d'une 
table et négocier et tout peut se négocier et la solution elle existe 

ce qu'il faut c'est c'est ceux qui vont qui vont la négocier et je pense 
que .. les politiques en France et en Europe ont un rôle à jouer à la 
table de négociation; il faut prendre part à ces négociations; 
il faut accompagner les Israéliens et les Palestiniens dans ce processus 
de l'essentielle réconciliation 

et même si aujourd'hui c'est très compliqué les douleurs des uns, ?? 
les douleurs de l'autre mais c'est on on est très tôt la guerre est encore 
la population est encore en train de se faire massacrer il y a des familles 
d'otages qui se demandent encore où sont leurs enfants où sont leurs 
compagnons où sont leurs frères leurs sœurs 

39'34" **on est encore dans le temps de l'effroi et de la sidération**
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=2374

donc on est encore dans le temps de l'effroi et de la sidération; 
on n'est pas encore dans le temps d'après.


39'39" en revanche je pense que nos  politiques ici en France et plus largement en Europe ont un rôle à jouer; les politiques français ont un rôle historique à jouer 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 
- https://youtu.be/j92qrPcM5-M?t=2379

En revanche je pense que nos politiques ici en France et plus largement 
en Europe ont un rôle à jouer; on pourra faire partie, on doit faire partie 
on doit être du côté de la paix; on a un rôle historique à jouer ; 
les politiques français ont un rôle historique à jouer 


39'56" Yuval Rahami : **ce qui s'est passé ce matin ça venait pas de la volonté du gouvernement israelien , ça venait de l'intervention internationale**
--------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=2396

vous juste une exemple ce qui s'est passé ce matin ok 

ce qui s'est passé ce matin ça venait pas de la volonté du gouvernement
israelien ok **ça venait de l'intervention internationale** et c'est c'est ça
la solution parce que la volonté du gouvernement isralien ne va ne va pas 
faire grand-chose à ce sujet 

Animateur
--------------------

deux questions 


40'28" Une femme:  sur les Arabes israéliens
------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=2427

Il y une population dont on on pas parlé et dont .. c'est des Arabes israéliens 

j'imagine que leur position à eux est très compliqué dans ce conflit 
certains, un grand nombre ont participé à aider les familles le 7 octobre ; 
ils ont été actifs les druzes aussi du reste et j'aurais voulu que tu 
nous parles un petit peu de leur position et aussi comment je crois que 
le gouvernement israélien est très injuste avec eux en fait tu nous en parle 
un petit peu 


41'06 Yuval Rahami : **les Palestiniens citoyens d'Israël pas comme avant ont décidé de ne pas prendre à côté palestinien dans ce conflit**
------------------------------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=2466


oui je vais pas parler du gouvernement israélien parce qu'on connait bien 
ce qu'il veut là mais par contre ce qui s'est passé en octobre je pense 
que Hamas a espéré que les Arabes Palestiniens citoyens d'Israël vont 
participer dans cette dans cette guerre, prendre le côté palestinien et 
ça ça s'est pas passé ; **les Palestiniens citoyens d'Israël pas comme avant
ont décidé de ne pas prendre à côté palestinien dans ce conflit** dans
cette guerre et tant que le gouvernement israélien a attendu que c'est 
qu'il y aura des manifestations, des violences dans les villes dans les 
villes ??? ou dans les villes arabes ça c'est ça c'est pas arrivé donc 
je pense que c'est admirable de la société palestinien israélien de faire 
cet acte très très responsable de pas ajouter l'huile sur ce feu et parce 
que eux aussi ils fortement touchés par cette guerre donc au nord d'Israël 
et dans les villages à côté de Gaza donc j'admire les sentiments Israéliens
Palestiniens de ne pas prendre côté 


.. _standing_together:

42'24" **"Standing together" (omdim beyachad)**
+++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=2544
- :ref:`raar_liens:standing_together`

Vous avez vu l'organisation "Standing together" (omdim beyachad) qui 
était organision Israëlo palestinien mais israéliens juifs, arabes israeliens

ok c'est pas ils ont jamais pris un très très fort pour les
Palestiniens en Cisjordanie ou à Gaza  sont c'est une organisation d'Israéliens
israéliens israéliens juifs et arabes 

ils sont très très forts dans cette maintenir cette sentiment arabes Israëliens 
et ils ont fait des des activités très très fortes en Israël dans toutes 
les villes pour parler contre la guerre et contre l'accusation des Arabes 
parce que les premières premières semaines de cette guerre on a cherché 
les Arabes hein pour les pour les attaquer mais eux ils ont fait beaucoup 
des événement des conférence dans toutes les villes pour dire on va pas 
laisser passer cette guerre en Israël dans Israël 

- https://youtu.be/j92qrPcM5-M?t=2613 (43'33")

43'32" Une femme: **Chypre c'est un endroit où tous les couples  israéliens qui peuvent pas se marier en Israël pour une raison pour une  autre vont se marier** 
--------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=2612


Ouis je voulais juste dire combien dans ces films et dans vos paroles 
qui sont dures qui sont tristes et qui sont terribles il y avait une 
chose qui m'a fait sourire c'est quand vous disiez que les rencontres 
entre les Arabes israéliens et les Arabes palestinien et Israéliens juifs 
se fait à Chypre parce que Chypre c'est un endroit où tous les couples 
israéliens qui peuvent pas se marier en Israël pour une raison pour une 
autre vont se marier en Chypre alors pas mariage tout de suite mais 
peut-êre une alliance 

[applaudissements]

44'21" Emmanuel Carroz
--------------------------------------

- https://youtu.be/j92qrPcM5-M?t=2661

On va passer la parole à monsieur le maire.



44'38" **Discours du maire de Grenoble Eric Piolle**
-----------------------------------------------------------

- https://youtu.be/j92qrPcM5-M?t=2678

Bonsoir à toutes et à tous 

Un tout petit mot pour conclure cette soirée en ce jour où un événement 
à Grenoble chaque année autour du tulipier du Jardin des Plantes planté 
en hommage à Yitzhak Rabin, en hommage à sa mort il y a 29 ans et nous 
gardons en tête aussi les  paroles qu'il avait prononcé juste avant son 
assassinat disant que il était convaincu qu'une majorité
du peuple souhaitait la paix et que c'est cela qu'il fallait entretenir.

Il avait toujours pensé cela ; nous sommes dans un moment évidemment
dramatique il y a une petite lueur d'espoir avec le cessez le feu au 
Liban cette nuit mais évidemment nous avons toutes et tous en tête ce qui se
passe à à Gaza et ce désastre à la fois de bombardement mais aussi de
situation sanitaire, de privation d'eau, d'alimentation, de destruction 
aussi des archives civiles, des cadastres tout ce qui fait l'histoire 
aussi 

et donc nos pensées depuis maintenant plus d'un an sont extrêmement sombres et
au milieu de cela il y a des démarches comme celle qui est portée dans
ce film, il y a des démarches comme celle portée par les guerrières de
la paix, comme celle portée par l'association israelo-palestinienne des 
familles endeuillées.

Merci beaucoup d'être là 

Il y a ces ces messages, d'abord des messages qui croient à la vie ;
des messages qui croient à la paix, qui rappellent que nous sommes toutes 
et tous les mêmes humains du même sang et ben ça fait du bien d'entendre 
ces messages 

Yitzhak Rabin, Yasser Arafat et  Shimon Peres avaient eu un prix Nobel de la paix
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://fr.wikipedia.org/wiki/Yasser_Arafat
- https://fr.wikipedia.org/wiki/Shimon_Peres

Yitzhak Rabin, Yasser Arafat et  Shimon Peres avaient eu un prix Nobel 
de la paix pour leur énergie pour trouver un chemin vers une paix qui 
semblait impossible; il y a des héritières et des héritiers qui entretiennent 
cela et je crois que c'est c'est très important pour nous toutes et nous tous. 


47'17" Grenoble aussi vous savez que nous sommes une des cinq  villes Compagnons de la Libération
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=2837
- https://www.cheminsdememoire.gouv.fr/fr/la-liberation-de-grenoble


Pour la ville de Grenoble aussi, vous savez que nous sommes une des cinq 
villes Compagnons de la Libération, que nous sommes là autour d'un ..
3 années de commémoration autour des 80 ans de la Libération d'abord 
de 43 et de la saint-barthémy Grenobloise 44 et la libération de 45 et 
la fin de la guerre le fait d'être une des villes compagnons de la Libération
et en cette année pour moi de coprésider l'ordre de la Libération 


47'52" résister et transmettre deux mots très actuels pour nous et qui nous font avancer à la fois dans notre lutte permanente contre l'antisémitisme qui est toujours vivace
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=2872

C'est aussi une responsabilité qui nous fait penser ce programme des 3 ans 
autour de **résister et transmettre**, résister et transmettre deux mots très 
actuels pour nous et qui nous font avancer à la fois dans **notre lutte 
permanente contre l'antisémitisme qui est toujours vivace** qui renait 
toujours partout, sous différentes formes mais toujours très puissant 
et s'adaptant à la situation nouvelle.


48'27" C'est aussi de cultiver la paix partout où c'est possible
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=2907

C'est aussi de cultiver la paix partout où c'est possible nous étions 
avec Emmanuel lundi dernier à Hiroshima où nous avons rencontré l'association 
qui reçoit cette année le Prix Nobel de la Paix 2024 

des moments aussi très forts et très poignants de tous ceux qui se dressent 
pour dire que le pire n'est pas le seul chemin 

et donc merci encore pour ce film et cette énergie on la  trouve chez des 
gens aussi comme  Vivian Silver |vivian| assassinée le 7 octobre par le Hamas 
on l'a trouvé dans dans son énergie irrésolue de tendre des ponts et 
puis aussi dans son fils [Yonatan Zeigen , NDLR] vous l'avez vu a choisi 
des mots extrêmement  forts pour dire que des choses nouvelle pouvaient 
naitre des destructions et pour choisir de donner le nom de sa mère pour 
un camp d'accueil dit préférer que son nom soit associé à protéger et 
accueillir les enfants plutôt que son nom soit marqué sur un obus visant 
à les tuer 

donc tout cela dans cette période très sombre il y a ces petites lueurs 
et la lueur ben suffit à faire disparaitre la nuit donc nous pouvons 
garder cela en tête, nous rattacher à ces lueurs en espérant toujours 
que une majorité du peuple cherche la paix et donc cultiver la peur
et la guerre n'est pas le seul chemin loin de là 

50'22" Entretenons collectivement cet espoir avec le Cercle Bernard Lazare |CblGrenoble|
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

- https://youtu.be/j92qrPcM5-M?t=3022


Entretenons collectivement cet espoir, entretenons là avec le cercle 
Bernard Lazare si si actif et avec toutes celles et ceux qui prennent 
le temps, l'énergie du dialogue et de la propagation de message de paix 
qui on l'espère, arrivera un jour un peu partout sur la planète mais 
particulièrement évidemment en Israël et en Palestine 

merci en tout cas à toutes et tous d'être là ce soir


50'59" Fin du discours du maire
+++++++++++++++++++++++++++++++++++++

https://youtu.be/j92qrPcM5-M?t=3059

[Applaudissements]


51'13" Emmanuel Caroz pour conclure
--------------------------------------

- https://youtu.be/j92qrPcM5-M?t=3073

Je conclue. 

Merci Monsieur le Maire, merci à toutes et tous pour  cette après-midi 
et cette soirée, puisque c'est depuis 15h30 que nous sommes ensemble. 

C'est une volonté, un souhait de la municipalité et de monsieur le maire 
de vous accueillir ici, notamment les associations qui sont là ce soir, 
il y a d'autres rendez-vous qui sont programmés, que nous programmons.
 
Voilà ça fait d'autant plus sens sur les 80 ans de la fin de la Seconde 
Guerre mondiale que voilà nous maintenons ces ponts ces ponts entre 
les continents ces ponts comme j'ai dit dans l'introduction c'est
des ponts entre générations 

Merci à vous toutes et à tous.

51'52" Fin
-----------------

- https://youtu.be/j92qrPcM5-M?t=3112 (51'52")
